## The scope of this project is to make rpi distributed application that gather rpi sensor data

## Folder incorporated/edited

1. sensehat_python3/sensors.py 

---

## Functions/Usecase incorporated/edited of specific application

1. Gather information ragarding surrounding temperature
2. Gather information regarding air pressure
3. Gather information regarding humidity
4. Gather information regarding memory
5. Gather information regarding disk space
6. Gather information regarding cpu temperature

---

## Additional System installed/orcestrated to run specific application

1. sense-hat
