#!/usr/bin/env python

"""
The Sense HAT has an integrated temperature sensor, which returns the temperature in Celsius degrees. 
The Sense HAT has a humidity sensor that returns relative humidity as a percentage, 
it uses the data from the temperature sensor to provide a correct value. 
There is also a barometric sensor integrated on the board which 
returns a pressure value expressed in millibars.
"""

from sense_hat import SenseHat

sense = SenseHat()
temp = round(sense.get_temperature(),2)
pressure = round(sense.get_pressure(),2)
humidity = round(sense.get_humidity(),2)

print("Temperature: %s C" % temp)
print("Humidity: %s %%rH" % humidity)
print("Pressure: %s Millibars" % pressure)

